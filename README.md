# How to test this file

```
source /cvmfs/clicdp.cern.ch/iLCSoft/builds/nightly/x86_64-slc6-gcc62-opt/init_ilcsoft.sh

ddsim --compactFile $lcgeo_DIR/ILD/compact/ILD_o1_v05/ILD_o1_v05.xml \
      -G -N 10 \
      --gun.distribution uniform \
      --gun.energy "10*GeV" \
      --gun.isotrop=True \
      -O muons.slcio

Marlin --global.LCIOInputFiles=muons.slcio \
       --constant.lcgeo_DIR=$lcgeo_DIR \
       --constant.DetectorModel=ILD_o1_v05 \
       --constant.OutputBaseName=tpc_reco \
       TPC_Reconstruction.xml
```